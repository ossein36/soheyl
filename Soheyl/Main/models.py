from django.db import models


class Permission(models.Model):
    role_en_name = models.CharField(max_length=128)
    role_fa_name = models.CharField(max_length=128)


class UserCustom(models.Model):
    first_name = models.CharField(max_length=128, null=True, blank=True, default=None)
    last_name = models.CharField(max_length=128, null=True, blank=True, default=None)
    id_number = models.CharField(max_length=11, unique=True)
    phone_number = models.CharField(max_length=11, unique=True)
    login_code = models.CharField(max_length=6, default=None, null=True)
    login_code_create_datetime = models.DateTimeField(default=None, null=None)
    last_action_datetime = models.DateTimeField(auto_created=True, auto_now=True, auto_now_add=True)

    website_address = models.URLField(default=None, null=True)
    user_permission = models.ForeignKey(Permission, on_delete=models.SET_DEFAULT, null=True, default=None)
    user_permissions = models.ManyToManyField(Permission)

    age = models.PositiveSmallIntegerField()
    trust_score = models.FloatField()
    description = models.TextField()
    is_active = models.BooleanField(default=True)
    ip = models.GenericIPAddressField()

